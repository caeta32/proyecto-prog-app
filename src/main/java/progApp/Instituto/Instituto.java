package progApp.Instituto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instituto")
public class Instituto {

	@Id
	@Column(name = "Nombre")
	private String nombreInstituto;
	
	public Instituto() {
		
	}
	
	public Instituto(String nombreInstituto) {
		super();
		this.nombreInstituto = nombreInstituto;
	}

	public String getNombreInstituto() {
		return nombreInstituto;
	}

	public void setNombreInstituto(String nombreInstituto) {
		this.nombreInstituto = nombreInstituto;
	}
	
	
}
