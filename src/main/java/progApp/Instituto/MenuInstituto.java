package progApp.Instituto;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JTextField;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class MenuInstituto extends JFrame {

    /**
     * 
     */
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField nombreInstituto;
    Connection con=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    VerInstitutos verTodosInst;
    ArrayList<String> myList = new ArrayList<>();
    public static JFrame frameList;
    @SuppressWarnings("rawtypes")
	public static JList list;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        emf = Persistence.createEntityManagerFactory("Institutos");
        manager = emf.createEntityManager();
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MenuInstituto frame = new MenuInstituto();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
    public void Dbconnection()
    {
       try{

               Class.forName("com.mysql.jdbc.Driver");
               con=DriverManager.getConnection("jdbc:mysql://sql10.freemysqlhosting.net/sql10363008","sql10363008","gRz9qgAB5m");            }
           catch(Exception e)
           {
               System.out.println("Error in connection"+e);
           }
   }
    
    public void getData()
    {
      try{
                ps=con.prepareStatement("select * from instituto");

                rs=ps.executeQuery();
                while(rs.next())
                {
                    System.out.println(rs.getString(1)); //here you can get data, the '1' indicates column number based on your query

                }

          }
          catch(Exception e)
          {
              System.out.println("Error in getData"+e);
          }

    }
    
    /**
     * Create the frame.
     */
    
    public MenuInstituto() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 515, 150);
        contentPane = new JPanel();
        contentPane.setPreferredSize(new Dimension(500, 500));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Registro Institucional");
        lblNewLabel.setBounds(78, 14, 134, 14);
        lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 14));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Nombre del Instituto");
        lblNewLabel_1.setBounds(84, 43, 139, 14);
        contentPane.add(lblNewLabel_1);

        nombreInstituto = new JTextField();
        nombreInstituto.setBounds(202, 40, 287, 20);
        contentPane.add(nombreInstituto);
        nombreInstituto.setColumns(10);

        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.setBounds(400, 71, 89, 23);
        btnAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Instituto registrado = new Instituto(nombreInstituto.getText());
                if (nombreInstituto.getText().equals("")) {
                    JOptionPane.showMessageDialog((Component) e.getSource(),
                        "Nombre Invalido",
                        "ERROR",
                        JOptionPane.INFORMATION_MESSAGE);
                } else {
                    if (manager.find(Instituto.class, registrado.getNombreInstituto()) == null) {
                        manager.getTransaction().begin();
                        manager.merge(registrado);
                        manager.getTransaction().commit();
                        JOptionPane.showMessageDialog((Component) e.getSource(),
                            "Instituto registrado correctamente",
                            "Operacion Exitosa",
                            JOptionPane.INFORMATION_MESSAGE);
                        nombreInstituto.setText("");

                    } else {
                        int res = JOptionPane.showOptionDialog(new JFrame(), "Desea modificar el nombre del Instituto?", "Instituto ya registrado",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                            new Object[] {
                                "Si",
                                "No"
                            }, JOptionPane.YES_OPTION);
                        if (res == JOptionPane.YES_OPTION) {
                            System.out.println("Si");
                            String modificado = JOptionPane.showInputDialog(new JFrame(), "Ingrese nuevo nombre de Instituto", null);
                            if (modificado.isEmpty() == true) {
                                JOptionPane.showMessageDialog((Component) e.getSource(),
                                    "Nombre Invalido",
                                    "ERROR",
                                    JOptionPane.INFORMATION_MESSAGE);
                            } else {
                                if (manager.find(Instituto.class, modificado) != null) {
                                    JOptionPane.showMessageDialog((Component) e.getSource(),
                                        "Nombre Invalido",
                                        "ERROR",
                                        JOptionPane.INFORMATION_MESSAGE);
                                } else {
                                    manager.getTransaction().begin();
                                    manager.remove(manager.contains(registrado) ? registrado : manager.merge(registrado));
                                    manager.getTransaction().commit();
                                    registrado = new Instituto(modificado);
                                    manager.getTransaction().begin();
                                    manager.merge(registrado);
                                    manager.getTransaction().commit();
                                    JOptionPane.showMessageDialog((Component) e.getSource(),
                                        "Instituto modificado correctamente",
                                        "Operacion Exitosa",
                                        JOptionPane.INFORMATION_MESSAGE);
                                }
                            }


                            nombreInstituto.setText("");
                        } else if (res == JOptionPane.NO_OPTION) {
                            nombreInstituto.setText("");
                        } else if (res == JOptionPane.CLOSED_OPTION) {
                            nombreInstituto.setText("");
                        }

                    }
                }


            }
        });
        contentPane.add(btnAceptar);

        JButton btnCancelar = new JButton("Salir");
        btnCancelar.setBounds(301, 71, 89, 23);
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                manager.close();
                dispose();
            }
        });
        contentPane.add(btnCancelar);
        
        JButton verTodos = new JButton("Ver todos");
        verTodos.setBounds(202, 71, 89, 23);
        verTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
        		verTodosInst = new VerInstitutos();
        		verTodosInst.setVisible(true);
        	}
        });
        contentPane.add(verTodos);
        ClassLoader classLoader = getClass().getClassLoader();
       JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setBounds(10, 11, 71, 60);
        ImageIcon myimage = new ImageIcon(classLoader.getResource("images/logo_transparent.png"));
        Image img1 = myimage.getImage();
        Image img2 = img1.getScaledInstance(lblNewLabel_2.getWidth(), lblNewLabel_2.getWidth(), Image.SCALE_SMOOTH);
        ImageIcon i = new ImageIcon(img2);
        lblNewLabel_2.setIcon(i);
        contentPane.add(lblNewLabel_2);
       
    }
}